<?php

namespace App\Controller;

use App\Entity\Families;
use App\Entity\Ong;
use App\Entity\User;
use App\Form\FamiliesType;
use App\Form\OngType;
use App\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/api" name="api")
 */
class RegisterController extends AbstractController
{
    /**
     * @Route("/user" name="user" methods="POST")
     */
    public function register(Request $request, EntityManagerInterface $manager, UserPasswordEncoderInterface $encoder)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user, [
            'csrf_protection' => false
        ]);

        $form->submit(
            json_decode($request->getContent(), true),
            false
        );

        if($form->isSubmitted() && $form->isValid()) {
            $hash = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hash);

            $manager->persist($user);
            $manager->flush();

            return $this->json($user, Response::HTTP_CREATED);
        }

        return $this->json($form->getErrors(true), 400);

    }

    /**
     * @Route("/families" name="families" methods="POST")
     */
    public function familiesRegister(Request $request, UserPasswordEncoderInterface $encoder, EntityManagerInterface $manager){
        $family = new Families();
        $form = $this->createForm(FamiliesType::class, $family, [
            'csrf_protection' => false
        ]);
        $form->submit(
            json_decode($request->getContent(), true),
            false
        );

        if($form->isSubmitted() && $form->isValid()) {
            $hash = $encoder->encodePassword($family, $family->getPassword());
            $family->setPassword($hash);

            $manager->persist($family);
            $manager->flush();

             return $this->json($family, Response::HTTP_CREATED);
        }
        
         return $this->json($form->getErrors(true), 400);
    }

    /**
     * @Route("/ong" name="ong" methods="POST")
     */
    public function ongRegister(Request $request, UserPasswordEncoderInterface $encoder, EntityManagerInterface $manager){
        $ong = new Ong();
        $form = $this->createForm(OngType::class, $ong, [
            'csrf_protection' => false
        ]);
        $form->submit(
            json_decode($request->getContent(), true),
            false
        );

        if($form->isSubmitted() && $form->isValid()) {
            $hash = $encoder->encodePassword($ong, $ong->getPassword());
            $ong->setPassword($hash);

            $manager->persist($ong);
            $manager->flush();

             return $this->json($ong, Response::HTTP_CREATED);
        }
        
         return $this->json($form->getErrors(true), 400);
    }
}